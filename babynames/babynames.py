#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""Baby Names exercise

Define the extract_names() function below and change main()
to call it.

For writing regex, it's nice to include a copy of the target
text for inspiration.

Here's what the html looks like in the baby.html files:
...
<h3 align="center">Popularity in 1990</h3>
....
<tr align="right"><td>1</td><td>Michael</td><td>Jessica</td>
<tr align="right"><td>2</td><td>Christopher</td><td>Ashley</td>
<tr align="right"><td>3</td><td>Matthew</td><td>Brittany</td>
...

Suggested milestones for incremental development:
 -Extract the year and print it
 -Extract the names and rank numbers and just print them
 -Get the names data into a dict and print it
 -Build the [year, 'name rank', ... ] list and print it
 -Fix main() to use the extract_names list
"""
from bs4 import BeautifulSoup
from os.path import exists
from sys import argv, exit


def extract_names(filename):
    """
    Given a file name for baby.html, returns a list starting with the year
    string followed by the name-rank strings in alphabetical order.
    ['2006', 'Aaliyah 91', Aaron 57', 'Abagail 895', ' ...]
    """
    with open(filename, 'r') as html:
        soup = BeautifulSoup(html)

    baby_names = [soup.find('h3', align='center').text[-4:]]
    for a in soup.find_all('tr', align='right'):
        rank, boy, girl = (b.text for b in a.find_all('td'))
        baby_names.append('{} {}'.format(boy, rank))
        baby_names.append('{} {}'.format(girl, rank))

    baby_names.sort()
    return baby_names


def main():
    if len(argv) not in [2, 3]:
        print('usage: [--summaryfile] file [file ...]')
        exit(1)

    # Notice the summary flag and remove it from args if it is present.
    summary = False
    if argv[1] == '--summaryfile':
        summary = True
        del argv[1]

    if exists(argv[1]):
        if summary:  # write to txt file
            with open('baby_summary.txt', 'w') as f:
                for name in extract_names(argv[1]):
                    f.write(''.join((name, '\n')))
            print('Summary file: \'baby_summary.txt\'')
        else:        # print to terminal
            [print(name) for name in extract_names(argv[1])]
        exit(0)
    else:
        print('File does not exist.')

if __name__ == '__main__':
    main()
